package com.dev.rentcar.UI.Activities.Home.Models;

import java.util.List;

public class CarCategoryData {

    List<CarCategory> data;

    public List<CarCategory> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "CarCategoryData{" +
                "data=" + data +
                '}';
    }
}
