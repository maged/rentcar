package com.dev.rentcar.UI.Activities.Login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.dev.rentcar.Data.Local.SharedpreferencesHelper;
import com.dev.rentcar.R;
import com.dev.rentcar.UI.Activities.Welcom.Welcome;
import com.dev.rentcar.Utils.ValidationUtils;
import com.dev.rentcar.databinding.ActivityLoginBinding;

public class Login extends AppCompatActivity implements View.OnClickListener {

    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        init();

    }

    private void init() {
        binding.continueTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.continueTv) {
            if (dataIsValid()) {
                SharedpreferencesHelper.setLogedInUserData(binding.userNameEt.getText().toString(), this);
                startActivity(new Intent(this, Welcome.class));
                finish();
            }
        }
    }

    private boolean dataIsValid() {
        String userName = binding.userNameEt.getText().toString();
        if (ValidationUtils.textIsEmptyValidate(userName)) {
            binding.userNameEt.setError(getString(R.string.errorMsgEmpty));
            return false;
        } else if (ValidationUtils.textIsNotValidLenghtInputType(userName)) {
            binding.userNameEt.setError(getString(R.string.erroMsgUserNameValidation));
            return false;
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}