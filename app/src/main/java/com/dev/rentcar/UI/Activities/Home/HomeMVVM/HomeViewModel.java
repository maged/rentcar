package com.dev.rentcar.UI.Activities.Home.HomeMVVM;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dev.rentcar.UI.Activities.Home.Models.CarCategory;
import com.dev.rentcar.UI.Activities.Home.Models.CarCategoryData;
import com.dev.rentcar.Utils.Utils;
import com.google.gson.Gson;

import java.util.List;

public class HomeViewModel extends ViewModel {

    public MutableLiveData<List<CarCategory>> carCategroyMutableLiveData;

    public void getCarCategoryListFromJson(Context context){
        carCategroyMutableLiveData=new MutableLiveData<>();

        String jsonFileString = Utils.getJsonFromAssets(context.getApplicationContext(), "CarCategories.json");
        CarCategoryData carCategoryData= new Gson().fromJson(jsonFileString, CarCategoryData.class);
        carCategroyMutableLiveData.setValue(carCategoryData.getData());
    }
}
