package com.dev.rentcar.UI.Activities.CarCategoryRecords.Interface;

import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Car;

public interface CarClick {
    void onCarClick(Car car);
}
