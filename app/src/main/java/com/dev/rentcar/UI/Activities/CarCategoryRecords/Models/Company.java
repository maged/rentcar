package com.dev.rentcar.UI.Activities.CarCategoryRecords.Models;

import java.util.ArrayList;
import java.util.List;

public class Company {

    String car_company;
    String logo;
    List<Car> carList;

    public Company(String car_company, String logo) {
        this.car_company = car_company;
        this.logo = logo;
    }

    public String getCar_company() {
        return car_company;
    }

    public String getLogo() {
        return logo;
    }

    public List<Car> getCarList() {
        return carList;
    }
    public void addCarToList(Car car){
        if (carList==null){
            carList=new ArrayList<>();
        }
        carList.add(car);
    }
}
