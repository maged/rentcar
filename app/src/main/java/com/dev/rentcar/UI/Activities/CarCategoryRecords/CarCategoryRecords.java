package com.dev.rentcar.UI.Activities.CarCategoryRecords;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dev.rentcar.R;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Adapters.CarsAdapter;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Adapters.CompanyCarsAdapter;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.CarCategoryRecordsMVVM.CarCategoryRecordsViewModel;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Interface.CarClick;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Car;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Company;
import com.dev.rentcar.UI.Activities.Home.Models.CarCategory;
import com.dev.rentcar.Utils.Utils;
import com.dev.rentcar.databinding.ActivityCarCategoryRecordsBinding;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import java.util.ArrayList;
import java.util.List;


public class CarCategoryRecords extends AppCompatActivity implements View.OnClickListener, CarClick {

    ActivityCarCategoryRecordsBinding binding;
    List<Car> carList;
    List<Company>companyList;
    CarCategory carCategory;
    CarsAdapter carsAdapter;
    CompanyCarsAdapter companyCarsAdapter;
    BottomSheetDialog rentBottomSheetDialog;
    CarCategoryRecordsViewModel carCategoryRecordsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityCarCategoryRecordsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
    }

    private void init(){


        binding.appBar.backImg.setOnClickListener(this);
        carCategoryRecordsViewModel=new ViewModelProvider(this).get(CarCategoryRecordsViewModel.class);

        carsAdapter=new CarsAdapter(this,this);
        binding.carRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        binding.carRecyclerView.setAdapter(carsAdapter);
        binding.listImg.setOnClickListener(this);
        binding.gridImg.setOnClickListener(this);
        getCarsList();
        setCategoryData();



    }
    private void getCarsList() {
        carList=new ArrayList<>();
        if (getIntent().hasExtra("carsList")){
            carList= (List<Car>) getIntent().getSerializableExtra("carsList");
            carsAdapter.setCarList(carList);
        }
        if (getIntent().hasExtra("carCategroy")){
            carCategory= (CarCategory) getIntent().getSerializableExtra("carCategroy");
        }
    }

    private void setCategoryData() {
        carsAdapter.setCarCategory(carCategory);

        String screenTitle=carCategory.getCategory()+" "+getString(R.string.details);
        binding.appBar.titleTv.setText(screenTitle);

        int id = getResources().getIdentifier(carCategory.getÏmage_name(), "drawable", getPackageName());
        Glide.with(this).load(ResourcesCompat.getDrawable(getResources(),id,null)).into(binding.carCategoryImg);

        String result=carList.size()+" "+getString(R.string.result);
        binding.numOfResultTv.setText(result);

        binding.carCategoryNameTv.setText(carCategory.getCategory());
        binding.carCategroyDescTv.setText(carCategory.getDescription());
    }



    @Override
    public void onClick(View v) {
        if (v==binding.appBar.backImg){
            super.onBackPressed();
        }else if (v==binding.listImg){
            binding.carRecyclerView.setVisibility(View.VISIBLE);
            binding.companyRecyclerView.setVisibility(View.GONE);

        }else if(v==binding.gridImg){
            getCompanyList();
            binding.carRecyclerView.setVisibility(View.GONE);
            binding.companyRecyclerView.setVisibility(View.VISIBLE);
        }

    }

    private void getCompanyList() {
        if (companyList==null){
            companyList=new ArrayList<>();

            carCategoryRecordsViewModel.getCompanywithCarsList(carList);
            carCategoryRecordsViewModel.comanyListMutableLiveData.observe(this, companies -> {
                if (companies!=null){
                    companyList=companies;
                }
            });

        }
        setCompanyCarList();
    }

    private void setCompanyCarList() {
        binding.companyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        companyCarsAdapter=new CompanyCarsAdapter(this,this);
        binding.companyRecyclerView.setAdapter(companyCarsAdapter);
        companyCarsAdapter.setCompanyList(companyList);

    }


    @Override
    public void onCarClick(Car car) {
        if (car.isIs_Available()){

            rentBottomSheetDialog=new BottomSheetDialog(this);
            View bottomSheetView= LayoutInflater.from(this).inflate(R.layout.rent_layout,findViewById(R.id.bottomSheetContainer));
            EditText numOfdays=bottomSheetView.findViewById(R.id.numDaysEt);
            TextView saveTv=bottomSheetView.findViewById(R.id.saveTv);
            SingleDateAndTimePicker singleDateAndTimePicker=bottomSheetView.findViewById(R.id.dateTimePicker);

            saveTv.setOnClickListener(v -> {
                if (numOfdays.getText().toString().isEmpty()){
                     numOfdays.setError(getString(R.string.enterNumOfDays));
                }else{
                    String []dateTimeString=Utils.stringSplite(singleDateAndTimePicker.getDate().toString()," ");
                    String date=dateTimeString[0]+" "+dateTimeString[1]+" "+dateTimeString[2]+" "+dateTimeString[5];
                    String time=dateTimeString[3];
                    car.setDate(date);
                    car.setTime(time);
                    car.setDays(numOfdays.getText().toString());
                    carCategoryRecordsViewModel.insertCarRentDB(car,this);
                    carCategoryRecordsViewModel.insertCarRentSuccess.observe(this,s -> {
                        Toast.makeText(this, ""+s, Toast.LENGTH_SHORT).show();
                        rentBottomSheetDialog.cancel();
                    });
                    carCategoryRecordsViewModel.insertCarRentError.observe(this,s -> {
                        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
                        rentBottomSheetDialog.cancel();
                    });
                }
            });
            rentBottomSheetDialog.setContentView(bottomSheetView);
            rentBottomSheetDialog.show();
        }else{
            Toast.makeText(this, getString(R.string.carNotAvilable), Toast.LENGTH_SHORT).show();
        }
    }
}