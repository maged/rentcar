package com.dev.rentcar.UI.Activities.CarCategoryRecords.Models;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Car;

import java.util.List;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface CarDao {
    @Insert
    Completable insertCar(Car car);
}
