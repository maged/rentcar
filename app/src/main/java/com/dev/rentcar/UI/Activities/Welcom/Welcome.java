package com.dev.rentcar.UI.Activities.Welcom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.dev.rentcar.Data.Local.SharedpreferencesHelper;
import com.dev.rentcar.R;
import com.dev.rentcar.UI.Activities.Home.Home;
import com.dev.rentcar.databinding.ActivityWelcomeBinding;

public class Welcome extends AppCompatActivity {

    ActivityWelcomeBinding binding;
    private static final int SPLASH_TIME_OUT = 3000;
    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityWelcomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        init();

    }
    private void init(){
        if (SharedpreferencesHelper.checkIfUserLogedIn(this)){
            String userName=SharedpreferencesHelper.getLogedInUser(this);
            String welcomString=getString(R.string.welcome)+" "+userName;
            binding.welcomeTv.setText(welcomString);
        }
        handler=new Handler();
        handler.postDelayed(runnable,SPLASH_TIME_OUT);
    }
    Runnable runnable= () -> {
        startActivity(new Intent(Welcome.this, Home.class));
        finish();
    };
}