package com.dev.rentcar.UI.Activities.Home;

import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import com.dev.rentcar.UI.Activities.CarCategoryRecords.CarCategoryRecords;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.CarCategoryRecordsMVVM.CarCategoryRecordsViewModel;

import com.dev.rentcar.UI.Activities.Home.Adapters.CarCategoryAdapter;
import com.dev.rentcar.UI.Activities.Home.HomeInterface.CategoryClick;
import com.dev.rentcar.UI.Activities.Home.HomeMVVM.HomeViewModel;
import com.dev.rentcar.UI.Activities.Home.Models.CarCategory;
import com.dev.rentcar.databinding.ActivityHomeBinding;

import java.io.Serializable;

public class Home extends AppCompatActivity implements CategoryClick {

    ActivityHomeBinding binding;
    HomeViewModel homeViewModel;
    CarCategoryAdapter carCategoryAdapter;
    CarCategoryRecordsViewModel carCategoryRecordsViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        init();
    }
    private void init(){
        carCategoryAdapter=new CarCategoryAdapter(this,this);
        binding.carCategoryRecyclerView.setAdapter(carCategoryAdapter);

        homeViewModel=new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.getCarCategoryListFromJson(this);

        homeViewModel.carCategroyMutableLiveData.observe(this, carCategories -> {
            if (carCategories.size()!=0){
                carCategoryAdapter.setCarCategoryList(carCategories);
            }
        });
    }

    @Override
    public void onCarategoryClick(CarCategory categoryObj) {
        carCategoryRecordsViewModel=new ViewModelProvider(this).get(CarCategoryRecordsViewModel.class);
        carCategoryRecordsViewModel.getCarDataList(this,categoryObj.getCategory());
        carCategoryRecordsViewModel.carListMutableLiveData.observe(this, cars -> {
            Intent intent=new Intent(Home.this, CarCategoryRecords.class);
            intent.putExtra("carsList", (Serializable) cars);
            intent.putExtra("carCategroy",categoryObj);
            startActivity(intent);
        });
    }
}