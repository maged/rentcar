package com.dev.rentcar.UI.Activities.CarCategoryRecords.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dev.rentcar.R;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Interface.CarClick;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Company;
import com.dev.rentcar.databinding.CompanyItemBinding;

import java.util.List;

public class CompanyCarsAdapter extends RecyclerView.Adapter<CompanyCarsAdapter.CompanyCarsAdapterViewHolder> {
    Context context;
    List<Company> companyList;
    CarClick carClick;

    public CompanyCarsAdapter(Context context, CarClick carClick) {
        this.context = context;
        this.carClick = carClick;
    }

    public void setCompanyList(List<Company> companyList) {
        this.companyList = companyList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CompanyCarsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CompanyItemBinding binding=CompanyItemBinding.inflate(LayoutInflater.from(context),parent,false);
        return new CompanyCarsAdapterViewHolder(binding.getRoot(),binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyCarsAdapterViewHolder holder, int position) {
        Company company=companyList.get(position);
//
        Glide.with(context).load(company.getLogo()).placeholder(ResourcesCompat.getDrawable(context.getResources(),R.drawable.ic_loading,null)).into(holder.binding.companyImg);
        holder.binding.companyNameTv.setText(company.getCar_company());
        String numberOfCars=company.getCarList().size()+" "+context.getString(R.string.car);
        holder.binding.numOfCarTv.setText(numberOfCars);

        CarListAdapter carListAdapter=new CarListAdapter(context,carClick);
        carListAdapter.setCarList(company.getCarList());
        holder.binding.carsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.binding.carsRecyclerView.setAdapter(carListAdapter);

        holder.binding.companyCardView.setOnClickListener(v -> {
            if (holder.binding.carsRecyclerView.getVisibility()==View.VISIBLE){
                holder.binding.carsRecyclerView.setVisibility(View.GONE);
            }else{
                holder.binding.carsRecyclerView.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (companyList==null){
            return 0;
        }
        return companyList.size();
    }

    public class CompanyCarsAdapterViewHolder extends RecyclerView.ViewHolder{
        CompanyItemBinding binding;
        public CompanyCarsAdapterViewHolder(@NonNull View itemView, CompanyItemBinding binding) {
            super(itemView);
            this.binding=binding;
        }
    }
}
