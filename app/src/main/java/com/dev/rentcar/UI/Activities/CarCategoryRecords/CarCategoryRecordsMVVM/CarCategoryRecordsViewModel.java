package com.dev.rentcar.UI.Activities.CarCategoryRecords.CarCategoryRecordsMVVM;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dev.rentcar.Data.Local.RoomDatabase.Mdatabase;
import com.dev.rentcar.R;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Car;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.CarData;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Company;
import com.dev.rentcar.Utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CarCategoryRecordsViewModel extends ViewModel {
    public MutableLiveData<List<Car>> carListMutableLiveData;
    public MutableLiveData<String>insertCarRentSuccess;
    public MutableLiveData<String>insertCarRentError;
    public MutableLiveData<List<Company>>  comanyListMutableLiveData;
    CompositeDisposable compositeDisposable=new CompositeDisposable();

    public void getCarDataList(Context context,String categoryName){
        carListMutableLiveData=new MutableLiveData<>();
        String jsonFileString = Utils.getJsonFromAssets(context.getApplicationContext(),"carsRecords.json");
        CarData carData=new Gson().fromJson(jsonFileString,CarData.class);
        filterData(carData.getData(),categoryName);
    }

    private void filterData(List<Car> carListData, String categoryName) {

        List<Car> carList=new ArrayList<>();
        Observable<List<Car>> observable= io.reactivex.rxjava3.core.Observable.fromArray(carListData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        observable.flatMap(Observable::fromIterable)
                .filter(car -> car.getStyle().equals(categoryName))
                .subscribe(new Observer<Car>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(@NonNull Car car) {
                        carList.add(car);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        carListMutableLiveData.setValue(carList);
                    }
                });
    }
    public void insertCarRentDB(Car car,Context context){
        insertCarRentSuccess=new MutableLiveData<>();
        insertCarRentError=new MutableLiveData<>();
        Mdatabase mdatabase=Mdatabase.getInstence(context);
        mdatabase.carDao().insertCar(car)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
               compositeDisposable.add(d);
            }

            @Override
            public void onComplete() {
               insertCarRentSuccess.setValue(context.getString(R.string.rentCarSuccess));
            }

            @Override
            public void onError(@NonNull Throwable e) {
                  insertCarRentError.setValue(context.getString(R.string.rentFailed));
            }
        });
    }

    public void getCompanywithCarsList(List<Car> carList){
        comanyListMutableLiveData=new MutableLiveData<>();
        List<Company> companyList=new ArrayList<>();
        Set<String> companyNameSet=new HashSet<>();
        for (Car car: carList){
            if(!(companyNameSet.contains(car.getCar_company()))){
                Company company=new Company(car.getCar_company(),car.getLogo());
                companyList.add(company);
            }
            companyNameSet.add(car.getCar_company());

        }
        for (Company company:companyList){
            for(Car car:carList){
                if (car.getCar_company().equals(company.getCar_company())){
                    company.addCarToList(car);
                }
            }
        }
        comanyListMutableLiveData.setValue(companyList);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
