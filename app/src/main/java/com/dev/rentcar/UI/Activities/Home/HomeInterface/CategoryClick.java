package com.dev.rentcar.UI.Activities.Home.HomeInterface;

import com.dev.rentcar.UI.Activities.Home.Models.CarCategory;

public interface CategoryClick {
     void onCarategoryClick(CarCategory category);
}
