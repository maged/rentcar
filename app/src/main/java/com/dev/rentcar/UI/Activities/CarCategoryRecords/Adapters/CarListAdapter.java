package com.dev.rentcar.UI.Activities.CarCategoryRecords.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dev.rentcar.R;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Interface.CarClick;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Car;
import com.dev.rentcar.databinding.CarListItemBinding;

import java.util.List;

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.CarListAdapterViewHolder> {
    Context context;
    CarClick carClick;
    List<Car> carList;

    public CarListAdapter(Context context, CarClick carClick) {
        this.context = context;
        this.carClick = carClick;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CarListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CarListItemBinding binding=CarListItemBinding.inflate(LayoutInflater.from(context),parent,false);
        return new CarListAdapterViewHolder(binding.getRoot(),binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CarListAdapterViewHolder holder, int position) {
        Car car=carList.get(position);
        holder.binding.carModelTv.setText(car.getCar_model());

        if (car.isIs_Available()){
            holder.binding.avilableTv.setText(context.getString(R.string.avilable));
        }else {
            holder.binding.avilableTv.setText(context.getString(R.string.unavilable));
        }
        holder.binding.carCardView.setOnClickListener(v -> carClick.onCarClick(car));
    }

    @Override
    public int getItemCount() {
        if (carList==null){
            return 0;
        }
        return carList.size();
    }

    public class CarListAdapterViewHolder extends RecyclerView.ViewHolder{
        CarListItemBinding binding;
        public CarListAdapterViewHolder(@NonNull View itemView, CarListItemBinding binding) {
            super(itemView);
            this.binding=binding;
        }
    }
}
