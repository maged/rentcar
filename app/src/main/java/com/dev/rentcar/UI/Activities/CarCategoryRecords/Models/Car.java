package com.dev.rentcar.UI.Activities.CarCategoryRecords.Models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "car_rent_table")
public class Car implements Serializable {

    @PrimaryKey(autoGenerate = true)
    int id;
    String car_company;
    String logo;
    String car_model;
    String style;
    String year;
    String price;
    String car_Img;
    boolean is_Available;
    //rent fields
    String startDate;
    String stratTime;
    String rentDays;

    public String getDays() {
        return rentDays;
    }

    public void setDays(String rentDays) {
        this.rentDays = rentDays;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return startDate;
    }

    public String getTime() {
        return stratTime;
    }

    public void setDate(String startDate) {
        this.startDate = startDate;
    }

    public void setTime(String stratTime) {
        this.stratTime = stratTime;
    }

    public String getCar_company() {
        return car_company;
    }

    public String getLogo() {
        return logo;
    }

    public String getCar_model() {
        return car_model;
    }

    public String getStyle() {
        return style;
    }

    public String getYear() {
        return year;
    }

    public String getPrice() {
        return price;
    }

    public String getCar_Img() {
        return car_Img;
    }

    public boolean isIs_Available() {
        return is_Available;
    }
}
