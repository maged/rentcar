package com.dev.rentcar.UI.Activities.CarCategoryRecords.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dev.rentcar.R;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Interface.CarClick;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Car;
import com.dev.rentcar.UI.Activities.Home.Models.CarCategory;
import com.dev.rentcar.databinding.CarItemBinding;

import java.util.ArrayList;
import java.util.List;

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.CarsAdapterViewHolder> {
    Context context;
    CarClick carClick;
    List<Car> carList;

    CarCategory carCategory;


    public CarsAdapter(Context context, CarClick carClick) {
        this.context = context;
        this.carClick = carClick;
    }

    public void setCarList(List<Car> carList) {
        if (carList==null){
            carList=new ArrayList<>();
        }
        this.carList = carList;
        notifyDataSetChanged();

    }

    public void setCarCategory(CarCategory carCategory) {
        this.carCategory = carCategory;
    }

    @NonNull
    @Override
    public CarsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CarItemBinding binding=CarItemBinding.inflate(LayoutInflater.from(context),parent,false);
        return new CarsAdapterViewHolder(binding.getRoot(),binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CarsAdapterViewHolder holder, int position) {
        Car car=carList.get(position);

        int id = context.getResources().getIdentifier(carCategory.getÏmage_name(), "drawable", context.getPackageName());
        Glide.with(context).load(car.getCar_Img()).placeholder(ResourcesCompat.getDrawable(context.getResources(),R.drawable.ic_loading,null)).error(ResourcesCompat.getDrawable(context.getResources(),id,null)).into(holder.binding.carImg);
        holder.binding.carModelTv.setText(car.getCar_model());
        holder.binding.carCompanyTv.setText(car.getCar_company());
        if (car.isIs_Available()){
            holder.binding.carAvilablelTv.setText(context.getString(R.string.avilable));
        }else{
            holder.binding.carAvilablelTv.setText(context.getString(R.string.unavilable));
        }
        holder.binding.carCardView.setOnClickListener(v -> carClick.onCarClick(car));
    }

    @Override
    public int getItemCount() {
        if (carList==null){
            return 0;
        }
        return carList.size();
    }

    public class CarsAdapterViewHolder extends RecyclerView.ViewHolder{
        CarItemBinding binding;
        public CarsAdapterViewHolder(@NonNull View itemView, CarItemBinding binding) {
            super(itemView);
            this.binding=binding;
        }
    }
}
