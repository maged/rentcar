package com.dev.rentcar.UI.Activities.Home.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dev.rentcar.R;
import com.dev.rentcar.UI.Activities.Home.HomeInterface.CategoryClick;
import com.dev.rentcar.UI.Activities.Home.Models.CarCategory;
import com.dev.rentcar.databinding.CarCategoryItemBinding;

import java.util.ArrayList;
import java.util.List;

public class CarCategoryAdapter extends RecyclerView.Adapter<CarCategoryAdapter.CarCategoryAdapterViewHolder> {
    Context context;
    CategoryClick categoryClick;
    List<CarCategory> carCategoryList;

    public CarCategoryAdapter(Context context, CategoryClick categoryClick) {
        this.context = context;
        this.categoryClick = categoryClick;
    }

    public void setCarCategoryList(List<CarCategory> carCategoryList) {
        if (carCategoryList==null){
            carCategoryList=new ArrayList<>();
        }
        this.carCategoryList = carCategoryList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CarCategoryAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CarCategoryItemBinding binding=CarCategoryItemBinding.inflate(LayoutInflater.from(context),parent,false);
        return new CarCategoryAdapterViewHolder(binding.getRoot(),binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CarCategoryAdapterViewHolder holder, int position) {
          CarCategory carCategory=carCategoryList.get(position);

          holder.binding.carCategoryName.setText(carCategory.getCategory());
          int id = context.getResources().getIdentifier(carCategory.getÏmage_name(), "drawable", context.getPackageName());
          holder.binding.carCategoryImg.setImageResource(id);
          holder.binding.carCategoryCard.setOnClickListener(v -> {
              Toast.makeText(context, context.getString(R.string.pleaseWaite), Toast.LENGTH_SHORT).show();
              categoryClick.onCarategoryClick(carCategory);
          });
    }

    @Override
    public int getItemCount() {
        if (carCategoryList==null){
            return 0;
        }
        return carCategoryList.size();
    }

    public class CarCategoryAdapterViewHolder extends RecyclerView.ViewHolder{

        CarCategoryItemBinding binding;
        public CarCategoryAdapterViewHolder(@NonNull View itemView, CarCategoryItemBinding binding) {
            super(itemView);
            this.binding=binding;
        }
    }
}
