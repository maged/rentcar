package com.dev.rentcar.UI.Activities.Splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.dev.rentcar.Data.Local.SharedpreferencesHelper;
import com.dev.rentcar.UI.Activities.Login.Login;
import com.dev.rentcar.UI.Activities.Welcom.Welcome;
import com.dev.rentcar.Utils.Language;
import com.dev.rentcar.databinding.ActivitySplashBinding;

public class Splash extends AppCompatActivity {

    ActivitySplashBinding binding;
    private static final int SPLASH_TIME_OUT = 3000;
    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.getInstance().loadLanguage(getResources(),"en");
        binding=ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        handler=new Handler();
        handler.postDelayed(runnable,SPLASH_TIME_OUT);
    }
    Runnable runnable= this::whereToNavigate;

    private void whereToNavigate() {
        if (SharedpreferencesHelper.checkIfUserLogedIn(this)){
            startActivity(new Intent(Splash.this, Welcome.class));
        }else{
            startActivity(new Intent(Splash.this, Login.class));
        }
        finish();
    }
}