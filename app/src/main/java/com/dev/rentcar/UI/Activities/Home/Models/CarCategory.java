package com.dev.rentcar.UI.Activities.Home.Models;

import java.io.Serializable;

public class CarCategory implements Serializable {
    String category;
    String ïmage_name;
    String description;

    public String getCategory() {
        return category;
    }

    public String getÏmage_name() {
        return ïmage_name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "CarCategory{" +
                "category='" + category + '\'' +
                ", ïmage_name='" + ïmage_name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
