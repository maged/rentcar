package com.dev.rentcar.Utils;


import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.Locale;

public class Language {
    private static Language instance = null;

    public static Language getInstance() {
        if (instance == null)
            instance = new Language();
        return instance;
    }
    public void loadLanguage(Resources resources, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        resources.updateConfiguration(config,resources.getDisplayMetrics());
    }

}
