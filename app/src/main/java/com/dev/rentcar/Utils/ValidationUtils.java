package com.dev.rentcar.Utils;

public class ValidationUtils {
    public static boolean textIsEmptyValidate(String textString){
        return textString.isEmpty();
    }
    public static boolean textIsNotValidLenghtInputType(String textString){
        return (!(textString.matches("[a-zA-Z ]+")) || textString.length() > 30);
    }
}
