package com.dev.rentcar.Data.Local;

import android.content.Context;
import android.content.SharedPreferences;



public class SharedpreferencesHelper {

    public static void setLogedInUserData(String userName, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstents.PREF_KEY_APP, 0); // 0 - for private mode
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AppConstents.PREF_KEY_APP_USER_NAME, userName);
        editor.apply();
    }
    public static void logout(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstents.PREF_KEY_APP, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AppConstents.PREF_KEY_APP_USER_NAME, "");
        editor.apply();
    }
    public static String getLogedInUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstents.PREF_KEY_APP, 0); // 0 - for private mode
        String user_name = preferences.getString(AppConstents.PREF_KEY_APP_USER_NAME, "");
        return user_name;
    }

    public static boolean checkIfUserLogedIn(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstents.PREF_KEY_APP, 0); // 0 - for private mode
        String userName = preferences.getString(AppConstents.PREF_KEY_APP_USER_NAME, "");
        return userName.length() != 0;
    }

}
