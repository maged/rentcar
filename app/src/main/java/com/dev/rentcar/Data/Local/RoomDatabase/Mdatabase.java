package com.dev.rentcar.Data.Local.RoomDatabase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.CarDao;
import com.dev.rentcar.UI.Activities.CarCategoryRecords.Models.Car;

@Database(entities = Car.class,version = 1)
public abstract class Mdatabase extends RoomDatabase {
    private static Mdatabase instance;
    public abstract CarDao carDao();

    public static synchronized Mdatabase getInstence(Context context){
        if (instance==null){
            instance= Room.databaseBuilder(context.getApplicationContext(),
                     Mdatabase.class,"m_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

}
